
## Name
Forkio
## Description
Forkio it's a usefull tool for developers. Flexible adaptive design, user friendly interface. Simple web page created with HTML, CSS and JS.
Use the menu link to jump between the sections.
## Authors and acknowledgment
Ingrid Novak 
## 
    1.Created repository
    2.Published site on github
    3.Made gulp
    4.Created sections present text,features, pricing 
Oleksandra Korkh 
## 
    1.Created sections header, testimonials
    2.Created README.md
Both
##
    Added adaptive design for a phone, tablet, computer;
## License
No-license
## Dependencies
        Gulp Local version: 4.0.2
        node v20.10.0
        npm 10.2.3
## Deployment
To run the program you have to run `gulp build --build`
Deployed via Github Pages on URL https://ingridnovak.github.io/

To deploy a new version you have to run `npm run deploy`